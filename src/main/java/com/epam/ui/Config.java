package com.epam.ui;

import java.awt.*;

/**
 * This is configuration class.
 */
public class Config {
    public static final int WIDTH = 10;
    public static final int HEIGHT = 20;
    public static final int SIZE = 20;
    public static final Color [] COLORS = {
            Color.GRAY,
            Color.CYAN,
            Color.GREEN};
}
